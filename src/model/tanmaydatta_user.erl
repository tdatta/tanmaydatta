-module(tanmaydatta_user, [Id, Email, Username, Password::string()]).
-compile(export_all).

-define(SETEC_ASTRONOMY, "Too many secrets to actually do anything here").

session_identifier() ->
    mochihex:to_hex(erlang:md5(?SETEC_ASTRONOMY ++ Id)).

check_password(PasswordAttempt) ->
    Password =:= bcrypt:hashpw(PasswordAttempt, Password).

login_cookies() ->
    [ mochiweb_cookies:cookie("tanmaydatta_user_id", Id, [{path, "/"}]),
        mochiweb_cookies:cookie("session_id", session_identifier(), [{path, "/"}]) ].
