-module(page, [Id, PageTitle, PageQualifier, PageLoc]).
-compile(export_all).
 
validation_tests() ->
    [{fun() -> length(PageTitle) > 0 end, "Page Title cannot be empty."},
     {fun() -> length(PageTitle) =< 10000 end, "Page Text cannot be more than 32 characters long."},
    {fun() -> length(PageLoc) > 0 end, "Page should have a source and cannot be empty."}].
