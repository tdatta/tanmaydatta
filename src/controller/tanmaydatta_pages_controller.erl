-module(tanmaydatta_pages_controller,  [Req, SessionId]).
-export([index/3,
	 view/2,
	 view_raw/2,
	 create/2]).
-default_action(index).
%% @doc show a list of all wiki pages out there
index('GET', [], ReqCtx) ->
    %% Req1 = proplists:get_value(request, ReqCtx),
    %% io:format("ip who requested it is ~p~n", [cowboy_req:header(<<"x-real-ip">>, Req1)]),
    Pages = boss_db:find(page, []),
    {ok, [{pages, Pages}]}.

%% @doc display a specific wiki page
view('GET', [PageQualifier]) ->
    lager:info("Page loc is ~p~n", [PageQualifier]),
    lager:info("response is ~p~n", [boss_db:find(page, [{page_qualifier, equals, PageQualifier}])]),
    case boss_db:find(page, [{page_qualifier, equals, PageQualifier}]) of
        {error, Reason} -> {redirect, [{action, "create"}]}; %% TODO: Redirect to error page
        undefined -> {redirect, [{action, "create"}]}; % When you visit /view/NotExistentPage the requested Page doesn't exist, we redirect the client to the edit Page so the content may be created
        [{page, Page_ID, Page_Title, Page_Qualifier, Page_Location}] -> 
	    %% {page,"page-1",<<"testing page">>, <<"this is just a new page">>}
						% Replace all [page-id] with links
						% TODO: There has to be a better way
            %% StartHrefs = re:replace(ExistingWikiPage:page_text(), "\[\w*-*[0-9]*", "<a href='/pages/view/&'>&", [global, {return, list}]),Testing_with_viju
            %% ClosedHrefs = re:replace(StartHrefs, "\]", "</a>", [global, {return, list}]),
            %% CleanedUp = re:replace(ClosedHrefs, <<"\[">>, "", [global, {return, list}]),
            %% {ok, [{page, ExistingWikiPage}, {cleaned, CleanedUp}]}
	    {ok, Bin_Content} = file:read_file(Page_Location),
	    Page_Content = unicode:characters_to_list(Bin_Content),
	    %% Test = "<h1> This is sample test </h1>",
            {ok, [{page, Page_ID}, {cleaned,Page_Content}]} 
            %% %% {ok, [{page, Page_ID}, {cleaned, Bin_Content}],[{"Content-Type", "HTML"}]}
    end.

view_raw('GET', [PageQualifier])->
    {ok, Bin_Content} = file:read_file(code:priv_dir(tanmaydatta) ++ "/raw/" ++ PageQualifier),
    {output, Bin_Content}.
%% @doc Handles rendering the new wiki page view which is empty by default
create('GET', []) -> ok;

%% @doc Handles POST data of form submission from new wiki page
create('POST', []) ->
    Title = Req:post_param("page_title"),
    Text = Req:post_param("page_text"),
    NewWikiPage = page:new(id, Title, Text),
    case NewWikiPage:save() of
        {ok, SavedWikiPage} ->   {redirect, [{action, "view"}, {id, SavedWikiPage:id()}]}; 
        {error, ErrorList} -> {ok, [{errors, ErrorList}, {new_page, NewWikiPage}]}
    end.
