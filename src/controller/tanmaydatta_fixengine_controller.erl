-module(tanmaydatta_fixengine_controller,  [Req, SessionId]).
-export([
         index/2
         ,test/2
         ,about/3
         ,list/2
         ,msgtype/2
        ]).
-default_action(index).
%% @doc show a list of all wiki pages out there

index('GET', []) ->
    {ok, nothing}.

test('GET',[])->
    {Value, _Rest} = test:test_decode1(),
    Plist = fixparser_utils:to_proplist(Value),
    {json, Plist}.

list('GET',[Query])->
    FixMap = fixparser_utils:get_consolidated_defs(#{fix_version=>fix50sp2, fix_transport=>fixt11}),
    Answer = maps:get(list_to_atom(Query), FixMap),

    {json, fixparser_utils:to_proplist(Answer)};
    %% {output, io_lib:format("~p", [Answer])};

list('GET',[Query, FixVer])->
    FixMap = fixparser_utils:get_consolidated_defs(#{fix_version=>list_to_atom(FixVer)}),
    Answer = maps:get(list_to_atom(Query), FixMap),
    {json, fixparser_utils:to_proplist(Answer)}.
    %% {output, io_lib:format("~p", [Answer] )}.
    
msgtype('GET',[MsgType,FixVer])->
    FixMap = fixparser_utils:get_consolidated_defs(#{fix_version=>list_to_atom(FixVer)}),
    lager:info("got Fixmap ~p",[FixMap]),
    {ok, Answer }= fixparser_utils:query_fix({messages, MsgType},FixMap),
    {json, fixparser_utils:to_proplist(Answer)}.

about('GET', [], ReqCtx) ->
     {output, "Site is maintained by Tanmay. It's my playground, which I will polish soon"}.
  
