-module(tanmaydatta_user_controller, [Req]).
-compile(export_all).

login('GET', []) ->
    {ok, [{redirect, Req:header(referer)}]};

login('POST', []) ->
    Username = Req:post_param("username"),
    case boss_db:find(tanmaydatta_user, [{username, 'equals', Username}]) of
        [TanmaydattaUser] ->
            case TanmaydattaUser:check_password(Req:post_param("password")) of
                true ->
                   {redirect, proplists:get_value("redirect",
                       Req:post_params(), "/"), TanmaydattaUser:login_cookies()};
                false ->
                    {ok, [{error, "Authentication error"}]}
            end;
        [] ->
            {ok, [{error, "Authentication error"}]}
    end.

register('GET', []) ->
    {ok, []};

register('POST', []) ->
    lager:info("post request is ~p ~n", [Req]),
    Email = Req:post_param("email"),
    Username = Req:post_param("username"),
    {ok, Password} = bcrypt:hashpw(Req:post_param("password"),element(2,bcrypt:gen_salt())),
    NewUser = tanmaydatta_user:new(id, Email, Username, Password),
    Result = NewUser:save(),
    {ok, [Result]}.
