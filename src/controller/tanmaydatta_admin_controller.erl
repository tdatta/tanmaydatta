-module(tanmaydatta_admin_controller,  [Req, SessionId]).
-export([main/3,
	about/3,
	contact/2,
        not_found/2,
        error_occured/2
        ]).
-default_action(main).

main('GET', [], ReqCtx) ->
    lager:info("Someone trying admin"),
    %% {RealIp, _Req1} = cowboy_req:header(<<"x-real-ip">>, Req),
    %% io:format("ip who requested it is ~p~n", [proplists:get_value(request, ReqCtx)]),
    %% Location = "http://google.com/search?q=getting+redirected+while+trying+to+hack",
    {output, "nothing here"}.
    %% {output, "lol nothing here!! try somewhere else "}.
  

about('GET', [], ReqCtx) ->
     {output, "Site is maintained by Tanmay. It's my playground, which I will polish soon"}.
  

contact('GET', [])->
    {output, "tanmay.datta86@gmail.com"}.

    
not_found('GET',[])->
    {ok, Bin_Content} = file:read_file("priv/pages/oops.html"),
    Page_Content = unicode:characters_to_list(Bin_Content),
    io:format("priting this ~p~n",[Page_Content]),
    %% Test = "<h1> This is sample test </h1>",
    {ok, [{page, "Not found"}, {cleaned,Page_Content}]}. 

error_occured('GET', [])->
    io:format("going to do something"),
    {ok, Bin_Content} = file:read_file("priv/pages/oops.html"),
    Page_Content = unicode:characters_to_list(Bin_Content),
    io:format("priting this ~p~n",[Page_Content]),
    %% Test = "<h1> This is sample test </h1>",
    {ok, [{page, "Something Really Bad happend here"}, {cleaned,Page_Content}]}. 
