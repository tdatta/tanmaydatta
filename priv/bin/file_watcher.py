#!/usr/bin/python
import time  
import sys
import os
from watchdog.observers import Observer  
from watchdog.events import PatternMatchingEventHandler 
from BeautifulSoup import BeautifulSoup as Soup

dest_dir = "/home/tanmay/tools/tanmaydatta/priv/pages/"

class OrgHandler(PatternMatchingEventHandler):
    patterns = ["*.html"]

    def process(self, event):
        """
        event.event_type 
            'modified' | 'created' | 'moved' | 'deleted'
        event.is_directory
            True | False
        event.src_path
            path/to/observed/file
        """
        # the file will be processed there
        try:
            print "trying {}".format(event.src_path)
            page = open(event.src_path).read()
            soup = Soup(page)
            body = soup.find('body')
            file_dest = os.path.join(dest_dir, event.src_path.split("/")[-1])
            with open(file_dest, 'w') as dest_file:
                print "writing file {}".format(file_dest)
                # import pdb
                # pdb.set_trace()
                dest_file.write(body.prettify("utf-8"))
        except Exception as e:
            print "I got {}".format(e)
        print "done"

    def on_modified(self, event):
        self.process(event)

    def on_created(self, event):
        self.process(event)


if __name__ == '__main__':
    args = sys.argv[1:]
    observer = Observer()
    watch_dest = args[0] if args else "/home/tanmay/staging/tanmaydatta/pages"
    print "watching {}".format(watch_dest)
    observer.schedule(OrgHandler(),
                      path=watch_dest)
    observer.start()

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()

    observer.join()
